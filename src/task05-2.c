#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define F_Name="C:\\input.txt";

int Count_file_symbl(FILE *File)
{
	int res = 0;
	char c;
	File = fopen(F_Name, "rt");
	while (c = fgetc(File)!=EOF)
	{
		if ((c != '\n') && (c != ' ') && (c != '	'))
			res++;
	}
	return res - 1;
}
int Count_Word(FILE *File)//������� ���������� ���� � ������, ������������� ���� ������ ������ � ���������
{
	int res = 0;
	File = fopen(F_Name, "rt");
	char c = fgetc(File);
	while (c != EOF)
	{
		while (((c == ' ') || (c == '\n') || (c == '\t')) && (c != EOF))
			c = fgetc(File);
		if ((c != ' ') && (c != '\t') && (c != '\n') && (c != EOF))
			res++;
		while (((c != ' ') && (c != '\t') && (c != '\n')) && (c != EOF))
			c = fgetc(File);
	}
	fclose(File);
	File = fopen(F_Name, "rt");
	return res;
}
int Count_znak(FILE *File)//������� ����� ���������� � ������
{
	int res = 0;
	File = fopen(F_Name, "rt");
	char c = fgetc(File);
	while (c != EOF)
	{
		if ((c == '\'') || (c == '\n') || (c == '-') || (c == '"') || (c == '(') || (c == ')') ||
			(c == '.') || (c == ',') || (c == ':') || (c == ';') || (c == '!') || (c == '?') || (c == ' '))
			res++;
		c = fgetc(File);
	}
	return res;
}
int Count_num(FILE *File)//������� ���������� ���� � ������
{
	int res = 0;
	File = fopen(F_Name, "rt");
	char c = fgetc(File);
	while ((c = fgetc(File)) != EOF)
	{
		if ((c >= '0') && (c <= '9'))
			res++;
	}
	fclose(File);
	File = fopen(F_Name, "rt");
	return res;
}
int Averege(FILE *File)//������� ������� ����� ����� (����� �����)
{
	int res;
	res = (int)(Count_file_symbl(File, F_Name) / File_Word(File, F_Name));
	return res;
}
char Most_char(FILE *File)//������� ��� ������ ����������� �������
{
	char res = 0;
	char buf[127];
	File = fopen(F_Name, "rt");
	char c, i, tmp = 0;
	for (i = 0; i < 127; i++)
		buf[i] = 0;
	while ((c = fgetc(File)) != EOF)
	{
		if ((c != ' ') && (c != '\n') && (c != '\0') && (c != '\t'))
			buf[c]++;
		c = fgetc(File);
	}
	for (i = 0; i < 127; i++)
		if (buf[i] > tmp)
		{
			res = i;
			tmp = buf[i];
		}
	return res;
}
int main()
{
	FILE *File;
	char Check = 0;
	while ((File = fopen(F_Name, "rt")) == 0)
	{
		puts("Program hasn't found this file\nTry again\n");
		fgets(F_Name, 9, stdin);
	}
	if ((Check = fgetc(File)) != EOF)
	{
		printf("Symbols: %d\n", Count_file_symbl(File));
		printf("Words: %d\n", Count_Word(File));
		printf("Znakov: %d\n", Count_znak(File));
		printf("Num: %d\n", Count_num(File));
		printf("Averege: %d\n", Averege(File));
		printf("Famous char: %c\n", Most_char(File));
	}
	else
		puts("Empty file");
	fclose(File);
	return 0;
}